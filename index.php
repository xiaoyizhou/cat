<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"> 
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>
<style>
body {
    
    background-color: black;
}

.cc{
     cursor:url('icon.png'), auto;
    background-image:url('red_cat.jpg');
    background-size: 1450px,800px;
    height: 100%;
    width: 100%;
    left:0;
    top:0;
    border: solid 3px black;
    position: fixed;
}
	@-webkit-keyframes fadeIn {
    0% {opacity: 0; /*初始状态 透明度为0*/}
    10%{opacity: 0.1;}
    20%{opacity: 0.2;}
    30%{opacity: 0.3;}
    40%{opacity: 0.4;}
    50% {opacity: 0.5; /*中间状态 透明度为0*/}
    70%{opacity: 0.7;}
    80%{opacity: 0.8;}
    90%{opacity: 0.9;}
    100% {opacity: 1; /*结尾状态 透明度为1*/}
	}

	@-webkit-keyframes fadeOut{
		0% {opacity: 0; /*初始状态 透明度为0*/}
    	10%{opacity: 0.1;}
    	20%{opacity: 0.2;}
    	30%{opacity: 0.3;}
    	40%{opacity: 0.4;}
    	50% {opacity: 0.7; /*中间状态 透明度为0*/}
    	70%{opacity: 0.4;}
    	80%{opacity: 0.3;}
    	90%{opacity: 0.2;}
    	100% {opacity: 0; /*结尾状态 透明度为1*/}
	}

p.title{
	font-family: "Arial","Sans-serif","Monospace";font-size: 65px;color:silver;letter-spacing: 0.1cm;position: absolute;

	-webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 1s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
p.subtitle{
	font-family: "Arial","Sans-serif","Monospace";font-size: 80px;color:gray;letter-spacing: 0.2cm;position: absolute;margin-top: 5.5cm;margin-left: 6.5cm;
	text-shadow: 9px 6px 5px black;
	-webkit-animation-name: fadeOut; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 

}
p.time{
	font-family: "Arial","Sans-serif","Monospace";font-size: 25px;color:black;letter-spacing: 0.15cm; margin-top: 5.5cm;margin-left: 2cm;
	text-shadow:3px 3px 5px silver;font-variant: small-caps;
}
p.enter{
	font-family: "Verdana","Sans-serif","Monospace";font-size: 30px;color:black;letter-spacing: 0.15cm;border: 1px solid black;text-transform: uppercase; 
	width: 150px;padding:0.2cm;
	
	-webkit-border-radius: 50px;
	border-radius: 50px;;
	text-align: center;position: fixed;
	-webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 1s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
    p.footer{
        font-family: "Arial","Sans-serif","Monospace";font-size: 14px;color:black;
        letter-spacing: 0.02cm;position: absolute;text-align: center;font-variant: small-caps;opacity: 0.8;
        text-shadow:2px 2px 5px black;

    }
</style>
</head>
<body>
<div class="cc">
    
<p class="subtitle" id="sub_first" style="display:none">I&nbsp;Love</p>
<p class="title" id="first" style="display:none">I&nbsp;Love...</p>
<p class="subtitle" id="sub_second" style="display:none">BiuBiu&nbsp;Pig</p>
<p class="title" id="second" style="display:none">BiuBiu&nbsp;Pig</p>
<p class="enter" id="three" style="display:none"><a href="apple.php" style="text-decoration:none;color:black">enter</a></p>
<p class="footer" id="footer">Copyright @ 2015 · All Rights Reserved · BiuBiu and MeowMeow</p>
<?php
$date=date("j M,Y");
$time=date("H:i:s A");
echo "<p class=time>$date</p>";
echo "<p class=time id=time></p>";
?>
<script>
setTimeout(function(){document.getElementById("first").style.display=''},100);
setTimeout(function(){document.getElementById("sub_first").style.display=''},500)
setTimeout(function(){document.getElementById("sub_first").style.display="none"},2500)
setTimeout(function(){document.getElementById("second").style.display=''},1000);
setTimeout(function(){document.getElementById("sub_second").style.display=''},1500);
setTimeout(function(){document.getElementById("sub_second").style.display='none'},3501);
var height=window.innerHeight/3+"px";
var middle=window.innerWidth/2.2+"px";
document.getElementById("first").style.marginTop=height;
document.getElementById("first").style.marginLeft="2cm";
document.getElementById("first").style.color="black";

var myVar=setInterval(function(){mytime()});
function mytime()
{
	var d = new Date();
	var str=d.toLocaleTimeString();
	var time=str.replace("EDT","")
	document.getElementById("time").innerHTML =time;
}
document.getElementById("time").style.marginTop="-6cm";
document.getElementById("time").style.marginLeft="15cm";
document.getElementById("time").style.fontSize="60px";



document.getElementById("second").style.marginLeft="7cm";
document.getElementById("second").style.marginTop="9cm";
document.getElementById("sub_second").style.marginLeft="11cm";
document.getElementById("sub_second").style.marginTop="8cm";

document.getElementById("three").style.marginTop="16.5cm";
document.getElementById("three").style.marginLeft=middle;
setTimeout(function(){document.getElementById("three").style.display=''},2000);

var l=window.innerWidth/3 +"px";
document.getElementById("footer").style.marginTop="18.5cm";
document.getElementById("footer").style.marginLeft=l;
</script>

</body>

</div>
</html>