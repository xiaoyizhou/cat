<?php
include ('template.php');
?>
<!DOCTYPE Html>
<html>
<head>
<meta charset="utf-8">
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>
<style>
  @-webkit-keyframes fadeIn {
    0% {opacity: 0; /*初始状态 透明度为0*/}
    10%{opacity: 0.1;}
    20%{opacity: 0.2;}
    30%{opacity: 0.3;}
    40%{opacity: 0.4;}
    50% {opacity: 0.5; /*中间状态 透明度为0*/}
    70%{opacity: 0.7;}
    80%{opacity: 0.8;}
    90%{opacity: 0.9;}
    100% {opacity: 1; /*结尾状态 透明度为1*/}
  }
  @-webkit-keyframes myfirst {
    0%   {left: -1cm; }
    100% {left: 0.7cm;}
  }
  p.blog{
  font-size: 20px;color:black;font-family: "Arial","Serif","Monospace";letter-spacing: 0.03cm; font-variant: small-caps;
  text-shadow:1px 1px 5px black;position: absolute;margin-left: 0cm;
     -webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 2.5s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
form{
	position: absolute;
	margin-left: 12cm;
	margin-top:6cm;
}
input{
	margin-left: 1.5cm;
	margin-top: 0.65cm;
	width:12cm;
	border: none;
	font-size: 15px;
	color:black;font-family: "Arial","Serif","Monospace";
	background-color: transparent;
  
}
textarea{
	font-size: 15px;
	color:black;font-family: "Arial","Serif","Monospace";
	border: none;
	overflow-y: scroll;
  background-color: #FDFDFD;
}
</style>
</head>
<body>
<img src="gray.gif" style="position:absolute;margin-left:20cm;margin-top:1.1cm;opacity:0.7">
<img src="fall.gif" style="position:absolute;margin-top:8cm;margin-left:-0.5cm;width:11cm">
<img src="cats.gif" style="position:absolute;margin-top:15cm;margin-left:6cm;width:5cm">
<img src="scribblesframe.png" style="position:absolute;width:18cm;margin-top:5cm;margin-left:11cm">
<img src="bird.png" style="position:absolute;width:8cm;margin-top:-0.5cm;margin-left:11cm">

<form action="memory.php" method="post" id="myform">
	<p class="blog">title: </p><input id="title"type="text" placeholder="Your Title Here"><br>
	<p class="blog">story: </p>
  <textarea id="content" placeholder="Your Story Here" maxlength="9999999999999" name="mystory"></textarea>
	<a href="#!"><p class="blog" style="margin-top:-2cm;" id="done" onclick="doit()">done</p></a><br><br><br>
	<a href="memory.php"><p class="blog" style="margin-top:-2cm;">back</p></a>
</form>
<script>
document.getElementById("content").style.marginLeft="2cm";
document.getElementById("content").style.marginTop="0.55cm";
document.getElementById("content").style.width="13cm";
document.getElementById("content").style.height="9.5cm";
function doit()
{
  if(document.getElementById("content").value=='')document.getElementById("content").placeholder="Sorry, It is Empty!"
  else document.getElementById("myform").submit();
}
</script>
</body>
</html>